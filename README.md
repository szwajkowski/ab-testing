# A/B testing #

Application is deployed on Openshift cloud and can be accessed via http://abtesting-szwajkowski.rhcloud.com/. In order to compute group for given user, send GET request to `/getUserGroup?id=#userId`.

### Requirements ###

Application is using Gradle as a build system, implemented with Spring Boot and Java 8. To build/run it, [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) and [Gradle](https://gradle.org/) are needed.

### Configuration ###

`config/application.yml` file holds groups configuration, which is fairly simple:
```
ab-groups:
    groups:
        - name: Grupa a 
          weight: 2
        - name: Grupa b 
          weight: 3
        - name: Grupa c 
          weight: 5
```
just modify names and weights of groups, or add next entries in a similar manner.

### Running application ###

As the application is using Spring Boot, you won't need any additional application server. It would be run on an embedded Tomcat server.

Run following commands from root project directory to:

 * assembly JAR package `gradle build`
 * run unit tests `gradle test`
 * run application with Spring Boot plugin `gradle bootRun`

In order to run JAR file, simply type in your console: `java -jar build/lib/abtesting-1.0.jar`.

### Performance tests ###

Performance tests results and description can be found in `perfTests` directory.

### Contact ###

* In case of any questions please contact the author by sending an e-mail on szwajkowskilukasz@gmail.com