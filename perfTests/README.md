# Performance tests result #

Performance tests were conducted with [JMeter 2.12](http://jmeter.apache.org).

### Test plans ###

JMeter test plan is available in `jmeterTestPlan/abtestingPerfTests.jmx` file.

 * For local instance (12GB RAM, i7-4710HQ 2.5 GHz)
    * 1000 users (threads), 100 times, GET requests for `/getUserGroup` with randomly generated userId
 * For openshift - small gear (512MB RAM, unknown CPU)
    * 500 users, 100 times, GET requests for `/getUserGroup` with randomly generated userId
 
### Results ###

Files with result can be found in `results/` directory.

 * openshift.csv contains results from openshift cloud, where application was run with `java -jar`
 * local_java.csv contains results from local instance, run with `java -jar`
 * local_gradle.csv contains results from local instance, run with Spring Boot plugin


|                  | local instance | openshift|
|----------------- | -------------- | -------------|
|average mean time | ~130 ms        | ~1000 ms|
|throughput        | ~5000 users/sec | ~300 users/sec|
 
