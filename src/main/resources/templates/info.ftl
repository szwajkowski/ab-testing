<!DOCTYPE html>

<html lang="en">

<body>
	<h1>A/B Testing application</h1>
	
	To calculate group for a given user please send GET request under <pre>/getUserGroup?id=#userId</pre> <br>
	
	Current configuration: ${configuration} <br>
	Unique users count:  ${usersCount}
</body>

</html>