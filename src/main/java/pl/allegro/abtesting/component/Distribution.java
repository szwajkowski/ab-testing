package pl.allegro.abtesting.component;

import java.util.List;

public interface Distribution {

	List<String> computeGroupsOrder();
}
