package pl.allegro.abtesting.component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.allegro.abtesting.component.configuration.ConfigurationProvider;
import pl.allegro.abtesting.component.configuration.Group;

@Component
public class ABTestingDistribution implements Distribution {

	private final ConfigurationProvider configurationProvider;

	private final Map<String, Integer> distribution;

	private int allUsersCount = 0;

	@Autowired
	public ABTestingDistribution(ConfigurationProvider provider) {
		this.configurationProvider = provider;
		this.distribution = this.configurationProvider
				.getGroups()
				.stream()
				.collect(Collectors.toMap(g -> g.getName(), g -> 0));
	}

	@Override
	public List<String> computeGroupsOrder() {
		List<String> groups = new LinkedList<String>();
		for (int i = 0; i < configurationProvider.getWeightsSum(); i++) {
			String groupName = calculateDistribution();
			groups.add(groupName);
			distribution.put(groupName, distribution.get(groupName) + 1);
			allUsersCount++;
		}
		return groups;
	}

	private String calculateDistribution() {
		int sum = configurationProvider.getWeightsSum();

		List<GroupWrapperWithDeviation> list = configurationProvider
				.getGroups()
				.stream()
				.map(group -> new GroupWrapperWithDeviation(group, calculateCurrentDeviation(group, sum)))
				.collect(Collectors.toList());

		double deviationsSum = list.stream()
				.mapToDouble(group -> group.getDeviation())
				.sum();

		Optional<GroupWrapperWithDeviation> smallest = list
				.stream()
				.map(group -> new GroupWrapperWithDeviation(group, calculateFutureDeviation(group,
						deviationsSum)))
				.reduce(ABTestingDistribution::getSmaller);

		return smallest.get().getName();
	}

	private double calculateCurrentDeviation(Group group, int sum) {
		return group.getPart() - ((double) distribution.get(group.getName()) / sum);
	}

	private double calculateFutureDeviation(GroupWrapperWithDeviation group, double devSum) {
		double dev = Math.abs(group.getPart()
				- ((double) (distribution.get(group.getName()) + 1) / (allUsersCount + 1)));

		return dev + devSum - group.getDeviation();
	}

	private static GroupWrapperWithDeviation getSmaller(GroupWrapperWithDeviation first,
			GroupWrapperWithDeviation second) {
		if (first.getDeviation() <= second.getDeviation()) {
			return first;
		}
		return second;
	}

	private class GroupWrapperWithDeviation {

		private final Group wrappedGroup;

		private final double deviation;

		public GroupWrapperWithDeviation(Group group, double deviation) {
			this.wrappedGroup = group;
			this.deviation = deviation;
		}

		public GroupWrapperWithDeviation(GroupWrapperWithDeviation group, double deviation) {
			this(group.wrappedGroup, deviation);
		}

		public double getPart() {
			return wrappedGroup.getPart();
		}

		public String getName() {
			return wrappedGroup.getName();
		}

		public double getDeviation() {
			return deviation;
		}

	}
}
