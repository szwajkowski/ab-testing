package pl.allegro.abtesting.component;

public interface UserMappingComponent {

	String getUserGroup(String userId);

	int getUsersCount();
}
