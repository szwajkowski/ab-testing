package pl.allegro.abtesting.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ABUserMappingComponent implements UserMappingComponent {

	private final List<String> groupsOrder;

	private final int groupsCount;

	private final Map<String, String> userToGroupMapping;

	private int uniqueUsersCounter;

	@Autowired
	public ABUserMappingComponent(Distribution distribution) {
		this.groupsOrder = distribution.computeGroupsOrder();
		this.groupsCount = groupsOrder.size();
		this.uniqueUsersCounter = 0;
		this.userToGroupMapping = new HashMap<String, String>(100000);
	}

	@Override
	public String getUserGroup(String userId) {
		String group = userToGroupMapping.get(userId);

		if (group == null) {
			group = getGroup(userId);
		}
		return group;
	}

	@Override
	public int getUsersCount() {
		return uniqueUsersCounter;
	}

	private synchronized String getGroup(String userId) {
		String group = userToGroupMapping.get(userId);
		if (group == null) {
			group = groupsOrder.get(uniqueUsersCounter++ % groupsCount);
			userToGroupMapping.put(userId, group);
		}
		return group;
	}
}
