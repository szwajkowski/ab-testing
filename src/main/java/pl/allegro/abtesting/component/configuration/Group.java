package pl.allegro.abtesting.component.configuration;

import org.springframework.util.StringUtils;

public class Group {

	private String name = null;

	private int weight = -1;

	private double part;

	public Group() {
	}

	// For unit tests purpose only
	public Group(String name, int weight) {
		this.name = name;
		this.weight = weight;
	}

	public void init(int sum) {
		part = (double) weight / sum;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("{name: ");
		sb.append(this.name).append(", weight: ").append(this.weight).append("}");
		return sb.toString();
	}

	public void validate() {
		if (weight == -1 || StringUtils.isEmpty(name)) {
			throw new IllegalStateException(
					"Configuration is invalid, weight should be greater than 0, and name cannot be empty");
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public double getPart() {
		return part;
	}
}
