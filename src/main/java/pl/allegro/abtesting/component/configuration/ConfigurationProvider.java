package pl.allegro.abtesting.component.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("ab-groups")
@Component
public class ConfigurationProvider {

	@NotEmpty
	private final List<Group> groups = new ArrayList<Group>();

	private int weightsSum;

	@PostConstruct
	private void init() {
		validate(groups);
		weightsSum = 1;
		if (groups.size() > 1) {
			weightsSum = groups.stream().mapToInt(Group::getWeight).sum();
		}
		groups.stream().forEach(g -> g.init(weightsSum));
	}

	public List<Group> getGroups() {
		return this.groups;
	}

	public int getWeightsSum() {
		return weightsSum;
	}

	private void validate(List<Group> groups) {
		groups.stream().forEach(g -> g.validate());
	}

}
