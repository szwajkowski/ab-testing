package pl.allegro.abtesting.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.allegro.abtesting.component.UserMappingComponent;
import pl.allegro.abtesting.component.configuration.ConfigurationProvider;

@Controller
public class HomePageController {

	@Autowired
	private ConfigurationProvider configuration;

	@Autowired
	private UserMappingComponent userMapping;

	@RequestMapping("/")
	public String getInformation(Map<String, Object> model) {
		model.put("configuration", configuration.getGroups().toString());
		model.put("usersCount", userMapping.getUsersCount());
		return "info";
	}
}
