package pl.allegro.abtesting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.allegro.abtesting.component.UserMappingComponent;

@Controller
public class GroupController {

	@Autowired
	private UserMappingComponent component;

	@RequestMapping(value = "/getUserGroup")
	public @ResponseBody String getUserGroup(String id) {
		return component.getUserGroup(id);
	}

	@RequestMapping(value = "/getUserGroup", params = "!id")
	public @ResponseBody String getMissingIdMessage() {
		return "You must pass id as query parameter";
	}
}
