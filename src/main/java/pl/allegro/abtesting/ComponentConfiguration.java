package pl.allegro.abtesting;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = { "pl.allegro.abtesting.component" })
public class ComponentConfiguration {

}
