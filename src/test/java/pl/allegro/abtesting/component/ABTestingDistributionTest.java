package pl.allegro.abtesting.component;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import pl.allegro.abtesting.component.configuration.ConfigurationProvider;
import pl.allegro.abtesting.component.configuration.Group;

import com.google.common.collect.Lists;

@RunWith(MockitoJUnitRunner.class)
public class ABTestingDistributionTest {

	private ABTestingDistribution tested;

	@Mock
	private ConfigurationProvider configurationProvider;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void properDistributionOfSingleGroup() {
		// given
		when(configurationProvider.getGroups()).thenReturn(initSingleGroup());
		when(configurationProvider.getWeightsSum()).thenReturn(1);
		tested = new ABTestingDistribution(configurationProvider);

		// when
		List<String> computedGroup = tested.computeGroupsOrder();

		// then
		assertThat(computedGroup, contains("a"));
	}

	@Test
	public void properDistributionOfTwoGroups() {
		// given
		when(configurationProvider.getGroups()).thenReturn(initTwoGroups());
		when(configurationProvider.getWeightsSum()).thenReturn(5);
		tested = new ABTestingDistribution(configurationProvider);

		// when
		List<String> computedGroup = tested.computeGroupsOrder();

		// then
		assertThat(computedGroup, contains("b", "a", "b", "a", "b"));
	}

	@Test
	public void properDistributionOfTwoEqualGroups() {
		// given
		when(configurationProvider.getGroups()).thenReturn(initTwoEqualGroups());
		when(configurationProvider.getWeightsSum()).thenReturn(4);
		tested = new ABTestingDistribution(configurationProvider);

		// when
		List<String> computedGroup = tested.computeGroupsOrder();

		// then
		assertThat(computedGroup, contains("b", "a", "b", "a"));
	}

	private ArrayList<Group> initTwoGroups() {
		Group groupA = new Group("a", 2);
		groupA.init(5);

		Group groupB = new Group("b", 3);
		groupB.init(5);
		return Lists.newArrayList(groupA, groupB);
	}

	private ArrayList<Group> initTwoEqualGroups() {
		Group groupA = new Group("a", 2);
		groupA.init(5);

		Group groupB = new Group("b", 2);
		groupB.init(4);
		return Lists.newArrayList(groupA, groupB);
	}

	private ArrayList<Group> initSingleGroup() {
		Group groupA = new Group("a", 5);
		groupA.init(5);

		return Lists.newArrayList(groupA);
	}

}
